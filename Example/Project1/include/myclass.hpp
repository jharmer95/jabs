#pragma once
#include <string>

class MyClass {
private:
  std::string name;
  int number;

public:
  MyClass();
  MyClass(int n, std::string s);

  int GetNumber();
  std::string GetName();
  void SetName(const std::string &s);
};