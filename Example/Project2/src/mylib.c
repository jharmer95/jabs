#include <math.h>
#include "mylib.h"

double FindHypotenuse(double a, double b) { return sqrt((a * a) + (b * b)); }
